const toml = require("toml");

const resources = require("../filesystem/resources");
const { collectContentsFromSource } = require("../filesystem/collectContents");
const { dataToHtml } = require("../conversion/dataToHtml");
const { htmlPage } = require("../html/htmlPage");
const { htmlOverview } = require("../html/htmlOverview");
const { saveHtml } = require("../filesystem/saveHtml");

function buildQuiz(source, title, overview) {
    if (overview === "") {
        overview = "index.html";
    }
    const contentsPerFile = collectContentsFromSource(source, "toml");
    
    const links = [];
    Object.keys(contentsPerFile).forEach(file => {
        const contents = contentsPerFile[file];
        const data = toml.parse(contents);
        
        const inlineCss = 
            resources.css("main") + "\n"
            + resources.css("quiz") + "\n"
            + resources.highlightCss("default");
        const inlineJs = resources.inlineJs();

        let pageTitle = data.title;
        if (title !== undefined) {
            pageTitle = `${pageTitle} &ndash; ${title}`;
        }

        const html = htmlPage(
            pageTitle,
            dataToHtml(data),
            inlineCss,
            inlineJs,
        );

        saveHtml(source.getDirectory(), file, html);
        links.push({ 
            url: `${file.replace(/\.[^/.]+$/, "")}.html`, 
            title: data.title 
        });
    });

    if (overview !== undefined) {
        const overviewHtml = htmlOverview(links);
        const overviewPage = htmlPage(title, overviewHtml, resources.css("main"), "");
        saveHtml(source.getDirectory(), overview, overviewPage);
    }
}

module.exports = {
    buildQuiz
};