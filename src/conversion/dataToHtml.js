const { htmlQuestion } = require("../html/htmlQuestion");
const hljs = require('highlight.js'); 
const markdown = require("markdown-it")({
    highlight: function (str, lang) {
        if (lang && hljs.getLanguage(lang)) {
          try {
            return '<pre class="hljs"><code>' +
                   hljs.highlight(lang, str, true).value +
                   '</code></pre>';
          } catch (__) {}
        }
    
        return '<pre class="hljs"><code>' + md.utils.escapeHtml(str) + '</code></pre>';
      }
});

function dataToHtml(data, questionOffset) {
    if (typeof(questionOffset) != Number || questionOffset < 0) {
        questionOffset = 0;
    }

    const {title, questions} = data;
    
    if (title === undefined || questions === undefined) {
        throw new Error("Invalid data: title and questions are required");
    }

    return questions
        .map(question => {
            question.description = markdown.render(question.description);
            question.tip = markdown.render(question.tip || "");
            question.answer = markdown.render(question.answer);
            question.options = question.options = question.options.map(o => markdown.render(o));
            return question;
        })
        .reduce(
                (html, question, i) => 
                    html + htmlQuestion(i + questionOffset, question), 
                ""
        );
}

module.exports = {
    dataToHtml,
};