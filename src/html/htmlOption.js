function htmlOption(name, index, option) {
    return `
    <div class="option">
        <label for="${name}-${index}">
        <input type="radio" value="${index}" name="choice" id="${name}-${index}"/>
            ${option}
        </label>
    </div>`;
}

module.exports = {
    htmlOption,
}