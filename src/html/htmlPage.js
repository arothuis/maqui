function htmlPage(title, contents, inlineCss, inlineJs) {
    return `
<!DOCTYPE html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${title || "Quiz"}</title>
    <style>${inlineCss}</style>
</head>
<body>
    <div class="container">
        <h1 class="title">${title || "Quiz"}</h1>
        ${contents}
    </div>
    <script>${inlineJs}</script>
</body>
`;
}

module.exports = {
    htmlPage,
}