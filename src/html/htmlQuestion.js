const { htmlOption } = require("./htmlOption");

function htmlQuestion(id, question) {
    const htmlOptions = question.options
        .map((option, i) => htmlOption(id, i, option))
        .join("");

    const answerId = question.options.indexOf(question.answer);

    return `
    <article class="question-${id}">
        <div class="description">
            <h2>Question ${id + 1}</h2>
            ${question.description}
        </div>
        <div class="quiz">
            <form class="quiz-form" data-answer="${answerId}">
                <div class="tip">${question.tip}</div>
                ${htmlOptions}
                <button type="submit">Answer</button>
            </form>
        </div>
    </article>`;
}

module.exports = {
    htmlQuestion,
}