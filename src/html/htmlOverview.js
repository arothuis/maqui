function htmlOverview(links) {
    return `
        <div class="overview">
            <ol>
            ${
                links.map(link => `<li><a href="${link.url}">${link.title}</a></li>\n`).join("")
            }
            </ol>
        </div>
    `
}

module.exports = {
    htmlOverview,
};