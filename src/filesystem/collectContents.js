const fs = require("fs");
const nodePath = require("path");

function collectContentsFromSource(source, extension) {
    if (source.isDirectory) {
        return collectContentsFromDirectory(source.path, extension);
    } else {
        return collectContentsFromFile(source.path, extension);
    }
}

function collectContentsFromDirectory(directory, extension) {
    const paths = fs.readdirSync(directory)
        .filter(hasExtension(extension));
    
    const contents = {};
    paths.forEach(path => {
        const absolutePath = nodePath.resolve(directory, path);
        contents[path] = fs.readFileSync(absolutePath, "UTF-8");
    });

    return contents;
}

function collectContentsFromFile(absolutePath, extension) {
    const path = nodePath.basename(absolutePath);

    const contents = {};
    if (!hasExtension(extension)(path)) {
        return contents;
    }
    contents[path] = fs.readFileSync(absolutePath, "UTF-8");
   
    return contents;
}

function hasExtension(extension) {
    return (name) => 
        nodePath.extname(name).slice(1).toLowerCase() === extension.toLowerCase();
}

module.exports = {
    collectContentsFromSource,
}