const fs = require("fs");
const path = require("path");
const { minify } = require("html-minifier");

function saveHtml(directory, file, html) {
    const fileName = file.replace(/\.[^/.]+$/, "");
    const filePath = path.resolve(directory, `${fileName}.html`);

    fs.writeFileSync(filePath, minify(html, {
        collapseBooleanAttributes: true,
        collapseWhitespace: true,
        minifyCSS: true,
        minifyJS: true,
    }));
}

module.exports = {
    saveHtml,
};