const path = require("path");
const fs = require("fs");

function dir() {
    return path.resolve(__dirname, "../../resources");
}
function nodeModules() {
    return path.resolve(__dirname, "../../node_modules");
}

function inlineJs() {
    return fs.readFileSync(`${dir()}/inline.js`, "UTF-8");
}

function css(name) {
    return fs.readFileSync(`${dir()}/${name}.css`, "UTF-8");
}

function highlightCss(name) {
    const file = path.resolve(nodeModules(), "highlight.js", "styles", name);
    return fs.readFileSync(`${file}.css`, "UTF-8");
}


module.exports = {
    css,
    inlineJs,
    highlightCss,
};