const nodePath = require("path");
const fs = require("fs");

const { Path } = require("../model/Path");

function normalizePath(path, cwd) {
    if (path === undefined || path === null) {
        return new Path(cwd, true);
    }

    let absolutePath = path;
    if (!nodePath.isAbsolute(path)) {
        absolutePath = nodePath.resolve(cwd, path);
    }

    if (!fs.existsSync(absolutePath)) {
        throw new Error(`Path "${absolutePath}" does not exist`);
    }

    if (fs.lstatSync(absolutePath).isDirectory()) {
        return new Path(absolutePath, true);
    }

    return new Path(absolutePath, false);
}

module.exports = {
    normalizePath,
};