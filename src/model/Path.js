const nodePath = require("path");

function Path(path, isDirectory) {
    this.path = path;
    this.isDirectory = isDirectory;

    this.getDirectory = () => {
        if (this.isDirectory) {
            return this.path;
        }

        return nodePath.dirname(this.path);
    }
};

module.exports = {
    Path,
}