#!/usr/bin/env node

const yargs = require("yargs");

const { normalizePath } = require("../src/filesystem/normalizePath");
const { buildQuiz } = require("../src/actions/buildQuiz");

yargs
    .scriptName("maqui")
    .usage("$0 <cmd> [args]")
    .command(
        "build [sourcePath]",
        "builds quiz based on specified file or directory",
        (yargs) => {
            yargs.option("o", {
                alias: "overview",
                type: "string",
                describe: "Add an overview page with a given name",
            });
            yargs.option("t", {
                alias: "title",
                type: "string",
                describe: "Add a title",
            });
            yargs.positional("sourcePath", {
                type: "string",
                describe: "the path to the file or directory of files to build"
            });
        },
        ({sourcePath, title, overview}) => {
            if (sourcePath === undefined) {
                yargs.showHelp();
                return;
            }
            const source = normalizePath(sourcePath, process.cwd());
            buildQuiz(source, title, overview);
        }
    )
    .help()
    .demandCommand()
    .argv;