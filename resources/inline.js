var $forms = document.getElementsByClassName("quiz-form");
for (var i = 0; i < $forms.length; i++) {
    var $form = $forms[i];
    $form.addEventListener("submit", function (event) {
        event.preventDefault();
        var data = new FormData(event.target);
        var choice = data.get("choice");

        var classNames = event.target.className.split(" ");
        if (choice === undefined || choice === null) {
            while (classNames.length > 1) {
                classNames.pop();
            }
            event.target.className = classNames.join(" ");
            return;
        }

        var answer = event.target.getAttribute("data-answer");
        if (answer === choice) {
            if (classNames.length === 1) {
                classNames.push("correct");
            } else {
                classNames[1] = "correct";
            }
        } else {
            if (classNames.length === 1) {
                classNames.push("incorrect");
            } else {
                classNames[1] = "incorrect";
            }
        }

        event.target.className = classNames.join(" ");
    });
}