module.exports = {
    FILE_1: "test1.toml",
    FILE_1_CONTENTS: 'title = "TEST1"',
    FILE_2: "test2.toml",
    FILE_2_CONTENTS: 'title = "TEST2"',
    FILE_3: "test3.toml",
    FILE_3_CONTENTS: 'hello world',
}