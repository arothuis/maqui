const nodePath = require("path");
const { expect } = require("chai");
const { Path } = require("../../../src/model/Path");
const { collectContentsFromSource } = require("../../../src/filesystem/collectContents");
const {FILE_1, FILE_1_CONTENTS, FILE_2, FILE_2_CONTENTS, FILE_3} = require("./testFiles");

describe("Collect contents from directory by file extension", function () {
    specify("an object of filenames and contents of files with specified extension", function () {
        const directory = nodePath.resolve(__dirname, "test-dir");
        const source = new Path(directory, true);
        const extension = "toml";

        const contents = collectContentsFromSource(source, extension);
        
        expect(contents[FILE_1]).to.equal(FILE_1_CONTENTS);
        expect(contents[FILE_2]).to.equal(FILE_2_CONTENTS);
        expect(contents[FILE_3]).to.be.undefined;
    });

    specify("an empty object if no files with extension found", function () {
        const directory = nodePath.resolve(__dirname, "test-dir");
        const source = new Path(directory, true);
        const extension = "xml";

        const contents = collectContentsFromSource(source, extension);
        
        expect(Object.keys(contents)).to.have.lengthOf(0);
    });
});

describe("Collect contents from file by file extension", function () {
    specify("an object of filename and contents of file with specified extension", function () {
        const file = nodePath.resolve(__dirname, "test-dir", FILE_1);
        const source = new Path(file, false);
        const extension = "toml";

        const contents = collectContentsFromSource(source, extension);
        
        expect(Object.keys(contents)).to.have.lengthOf(1);
        expect(contents[FILE_1]).to.equal(FILE_1_CONTENTS);
    });

    specify("an empty object if no files with extension found", function () {
        const file = nodePath.resolve(__dirname, "test-dir", FILE_1);
        const source = new Path(file, false);
        const extension = "xml";

        const contents = collectContentsFromSource(source, extension);
        
        expect(Object.keys(contents)).to.have.lengthOf(0);
    });
});
