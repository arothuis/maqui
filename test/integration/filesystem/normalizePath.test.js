const { expect } = require("chai");
const nodePath = require("path");
const { normalizePath } = require("../../../src/filesystem/normalizePath");

const { FILE_1 } = require("./testFiles");

describe("Normalize path", () => {
    specify("error if absolute path does not exist", () => {
        const path = `${__dirname}/noop/${FILE_1}`;
        const cwd = __dirname;

        const nonExistanceFailure = () => normalizePath(path, cwd);

        expect(nonExistanceFailure).to.throw(path);
    });

    specify("error if relative path does not exist", () => {
        const path = `/noop/${FILE_1}`;
        const cwd = __dirname;

        const nonExistanceFailure = () => normalizePath(path, cwd);

        expect(nonExistanceFailure).to.throw(path);
    });

    specify("current working directory if no path is given", function () {
        const path = undefined;
        const cwd = __dirname;

        const normalizedPath = normalizePath(path, cwd);

        expect(normalizedPath.path).to.equal(cwd);
        expect(normalizedPath.isDirectory).to.be.true;
    });

    specify("absolute file path if absolute path to file is given", function () {
        const path = `${__dirname}/test-dir/${FILE_1}`;
        const cwd = __dirname;

        const normalizedPath = normalizePath(path, cwd);

        expect(normalizedPath.path).to.equal(path);
        expect(normalizedPath.isDirectory).to.be.false;
    });

    specify("absolute directory path if absolute path to directory is given", function () {
        const path = `${__dirname}/test-dir`;
        const cwd = __dirname;

        const normalizedPath = normalizePath(path, cwd);

        expect(normalizedPath.path).to.equal(path);
        expect(normalizedPath.isDirectory).to.be.true;
    });

    specify("absolute file path if relative path to file is given", function () {
        const path = `test-dir/${FILE_1}`;
        const cwd = __dirname;

        const normalizedPath = normalizePath(path, cwd);

        expect(normalizedPath.path).to.equal(nodePath.resolve(cwd, path));
        expect(normalizedPath.isDirectory).to.be.false;
    });

    specify("absolute file path if relative path to file is given", function () {
        const path = "test-dir";
        const cwd = __dirname;

        const normalizedPath = normalizePath(path, cwd);

        expect(normalizedPath.path).to.equal(nodePath.resolve(cwd, path));
        expect(normalizedPath.isDirectory).to.be.true;
    });
});