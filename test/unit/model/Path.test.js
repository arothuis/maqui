const { expect } = require("chai");

const { Path } = require("../../../src/model/Path");

describe("Path", function() {
    specify("can provide its directory if it refers to a directory", function() {
        const dirPath = new Path(__dirname, true);
        const dir = dirPath.getDirectory();
        expect(dir).to.equals(__dirname);
    });

    specify("can provide its directory if it refers to a file", function() {
        const filePath = new Path(__dirname + "/somefile.txt", false);
        const dir = filePath.getDirectory();
        expect(dir).to.equals(__dirname);
    });
});